package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionDTORepository extends ExtendedDTORepository<SessionDTO> {

    long countByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<SessionDTO> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<SessionDTO> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<SessionDTO> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}

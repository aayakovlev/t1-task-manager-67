package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class TaskDTOServiceImpl extends AbstractExtendedDTOService<TaskDTO, TaskDTORepository>
        implements TaskDTOService {

    @Getter
    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        return getRepository().save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return getRepository().save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final TaskDTO resultTask = findById(userId, id);
        resultTask.setStatus(status);
        update(resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findAllByUserIdAndProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        getRepository().deleteAllByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().countByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Comparator<TaskDTO> comparator)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (comparator == null) return findAll(userId);
        @NotNull final Sort sort = getSort(comparator);
        return getRepository().findAllByUserId(userId, sort);
    }

    @NotNull
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<TaskDTO> resultEntity = getRepository().findByUserIdAndId(userId, id);
        if (!resultEntity.isPresent()) throw new TaskNotFoundException();
        return resultEntity.get();
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final TaskDTO model) throws AbstractException {
        if (model == null) throw new TaskNotFoundException();
        removeById(model.getUserId(), model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new TaskNotFoundException();
        getRepository().deleteByUserIdAndId(userId, id);
    }

}

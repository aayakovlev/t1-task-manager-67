package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.ITaskEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.aayakovlev.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskDTOService service;

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws AbstractException {
        return service.count();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void deleteAll() {
        service.deleteAll();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/exists/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() throws AbstractException {
        return service.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/save")
    public TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull final TaskDTO task
    ) throws EntityEmptyException {
        return service.save(task);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save/{id}")
    public TaskDTO update(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull final TaskDTO task
    ) throws EntityEmptyException {
        return service.save(task);
    }

}

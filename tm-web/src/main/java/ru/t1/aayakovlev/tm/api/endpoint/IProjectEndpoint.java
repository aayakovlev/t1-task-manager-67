package ru.t1.aayakovlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    long count() throws AbstractException;

    @WebMethod
    void deleteAll() throws AbstractException;

    @WebMethod
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull final String id) throws AbstractException;

    @WebMethod
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<ProjectDTO> findAll() throws AbstractException;

    @NotNull
    @WebMethod
    ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @NotNull final ProjectDTO project
    ) throws EntityEmptyException;

    @NotNull
    @WebMethod
    ProjectDTO update(
            @WebParam(name = "project", partName = "project")
            @NotNull final ProjectDTO project
    ) throws EntityEmptyException;

}

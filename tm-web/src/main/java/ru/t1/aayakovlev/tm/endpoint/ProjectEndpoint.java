package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.IProjectEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.aayakovlev.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectDTOService service;

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws AbstractException {
        return service.count();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void deleteAll() throws AbstractException {
        service.deleteAll();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/exists/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() throws AbstractException {
        return service.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/add")
    public ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final ProjectDTO project
    ) throws EntityEmptyException {
        return service.save(project);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save/{id}")
    public ProjectDTO update(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final ProjectDTO project
    ) throws EntityEmptyException {
        return service.save(project);
    }

}

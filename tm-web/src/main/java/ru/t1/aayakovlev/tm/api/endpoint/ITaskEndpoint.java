package ru.t1.aayakovlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    long count() throws AbstractException;

    @WebMethod
    void deleteAll();

    @WebMethod
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull final String id
    ) throws AbstractException;

    @WebMethod
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<TaskDTO> findAll() throws AbstractException;

    @NotNull
    @WebMethod
    TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @NotNull final TaskDTO task
    ) throws EntityEmptyException;

    @NotNull
    @WebMethod
    TaskDTO update(
            @WebParam(name = "task", partName = "task")
            @NotNull final TaskDTO task
    ) throws EntityEmptyException;

}

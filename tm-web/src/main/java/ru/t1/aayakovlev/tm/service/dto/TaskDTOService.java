package ru.t1.aayakovlev.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.repository.TaskDTORepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskDTOService implements ITaskDTOService {

    @Getter
    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @Override
    public long count() throws AbstractException {
        return getRepository().count();
    }

    @Override
    @Transactional
    public void deleteAll() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new TaskNotFoundException();
        getRepository().deleteById(id);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsById(id);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() throws AbstractException {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findAllByProjectId(projectId);
    }

    @NotNull
    @Override
    public TaskDTO findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<TaskDTO> resultEntity = getRepository().findById(id);
        if (!resultEntity.isPresent()) throw new TaskNotFoundException();
        return resultEntity.get();
    }

    @NotNull
    @Override
    public TaskDTO save(@Nullable final TaskDTO task) throws EntityEmptyException {
        if (task == null) throw new EntityEmptyException();
        return getRepository().save(task);
    }

}

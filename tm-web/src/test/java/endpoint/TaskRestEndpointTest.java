package endpoint;

import marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.client.TaskRestEndpointClient;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;

import java.util.List;

import static constant.TaskTestConstant.*;
import static constant.TaskTestConstant.TASK_THREE;

@Category(IntegrationCategory.class)
public final class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @Before
    public void init() {
        client.save(TASK_ONE);
        client.save(TASK_TWO);
        client.save(TASK_THREE);
    }

    @After
    public void finish() {
        client.deleteAll();
    }

    @Test
    public void When_Count_Expect_Value() {
        client.save(TASK_FOUR);
        long count = client.count();
        Assert.assertEquals(4, count);
    }

    @Test
    public void When_DeleteAll_Expect_NoEntities() {
        client.deleteAll();
        long count = client.count();
        Assert.assertEquals(0, count);
    }

    @Test
    public void When_DeleteById_Expect_NullEntity() {
        client.deleteById(TASK_ONE.getId());
        Assert.assertThrows(TaskNotFoundException.class,
                () -> client.findById(TASK_ONE.getId())
        );
    }

    @Test
    public void When_ExistsById_Expect_True() {
        final boolean result = client.existsById(TASK_TWO.getId());
        Assert.assertTrue(result);
    }

    @Test
    public void When_FindAll_Expect_Entities() {
        @NotNull final List<TaskDTO> results = client.findAll();
        Assert.assertNotEquals(0, results.size());
    }

    @Test
    public void When_FindById_Expect_ExistedEntity() {
        @NotNull final TaskDTO result = client.findById(TASK_TWO.getId());
        Assert.assertEquals(TASK_TWO.getName(), result.getName());
    }

    @Test
    public void When_Save_Expect_NewEntity() {
        client.save(TASK_FOUR);
        @NotNull final TaskDTO newTask = client.findById(TASK_FOUR.getId());
        Assert.assertEquals(TASK_FOUR.getName(), newTask.getName());
    }

    @Test
    public void When_Update_Expect_UpdatedEntity() {
        @NotNull final TaskDTO existedTask = client.findById(TASK_THREE.getId());
        existedTask.setName("new project name");
        client.update(existedTask);
        @NotNull final TaskDTO updatedTask = client.findById(TASK_THREE.getId());
        Assert.assertEquals("new project name", updatedTask.getName());
    }

}

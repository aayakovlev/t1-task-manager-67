package ru.t1.aayakovlev.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@Nullable final Role role) {
        if (role == null) return "";
        return role.getDisplayName();
    }

    @Nullable
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        return Arrays.stream(values())
                .filter((s) -> value.equals(s.name()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}

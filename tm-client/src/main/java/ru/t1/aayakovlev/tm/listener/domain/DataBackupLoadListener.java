package ru.t1.aayakovlev.tm.listener.domain;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataBackupLoadRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;


@Component
public final class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-backup-load";

    @NotNull
    private static final String DESCRIPTION = "Load backup data.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataBackupLoadListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        domainEndpoint.backupLoad(new DataBackupLoadRequest(getToken()));
    }

}

package ru.t1.aayakovlev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.ProjectUpdateByIdRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class ProjectUpdateByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Update project by id.";

    @NotNull
    public static final String NAME = "project-update-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        System.out.print("Enter new name: ");
        @NotNull final String name = nextLine();
        System.out.print("Enter new description: ");
        @NotNull final String description = nextLine();

        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);

        projectEndpoint.updateProjectById(request);
    }

}

package ru.t1.aayakovlev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.ProjectCreateRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    public static final String NAME = "project-create";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectCreateListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        @NotNull final String name = nextLine();
        System.out.print("Enter description: ");
        @NotNull final String description = nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);

        projectEndpoint.createProject(request);
    }

}
